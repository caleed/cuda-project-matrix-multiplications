#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cublas_v2.h>
#include <curand.h>
#include <algorithm>
#include <cassert>
#include <functional>
#include <vector>
#include <omp.h>

using namespace std;
using std::generate;
using std::vector;

//Matrix size
const int N = 2048;

//shared memory tile size
const int SHMEM_SIZE = 1024;


void cpu_matrix_mult(float *h_a, float *h_b, float *h_result, int N) {
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			float tmp = 0.0;
			for (int h = 0; h < N; ++h)
			{
				tmp += h_a[i * N + h] * h_b[h * N + j];
			}
			h_result[i * N + j] = tmp;
		}
	}
}


// Fill the array A(nr_rows_A, nr_cols_A) with random numbers on GPU
void GPU_fill_rand(float *A, int N) {
	// Create a pseudo-random number generator
	curandGenerator_t prng;
	curandCreateGenerator(&prng, CURAND_RNG_PSEUDO_DEFAULT);

	// Set the seed for the random number generator using the system clock
	curandSetPseudoRandomGeneratorSeed(prng, (unsigned long long) clock());

	// Fill the array with random numbers on the device
	curandGenerateUniform(prng, A, N * N);
}




// Multiply the arrays A and B on GPU and save the result in C BY CUBLAS
void gpu_blas_mmul(const float *A, const float *B, float *C, const int N) {
	int lda = N, ldb = N, ldc = N;
	const float alf = 1;
	const float bet = 0;
	const float *alpha = &alf;
	const float *beta = &bet;

	// Create a handle for CUBLAS
	cublasHandle_t handle;
	cublasCreate(&handle);

	// Do the actual multiplication
	cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, N, N, N, alpha, A, lda, B, ldb, beta, C, ldc);

	// Destroy the handle
	cublasDestroy(handle);
}




//Matrix Multiplication on GPU with shared memory//
__global__ void matrixMul(const float *a, const float *b, float *c) {
	
	// Compute each thread's global row and column index
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int col = blockIdx.x * blockDim.x + threadIdx.x;

	// Statically allocated shared memory
	__shared__ float s_a[SHMEM_SIZE];
	__shared__ float s_b[SHMEM_SIZE];

	// Accumulate in temporary variable
	float tmp = 0;

	// Sweep tile across matrix
	for (int i = 0; i < N; i += blockDim.x) {
		// Load in elements for this tile
		s_a[threadIdx.y * blockDim.x + threadIdx.x] = a[row * N + i + threadIdx.x];
		s_b[threadIdx.y * blockDim.x + threadIdx.x] =
			b[i * N + threadIdx.y * N + col];

		// Wait for both tiles to be loaded in before doing computation
		__syncthreads();

		// Do matrix multiplication on the small matrix
		for (int j = 0; j < blockDim.x; j++) {
			tmp +=
				s_a[threadIdx.y * blockDim.x + j] * s_b[j * blockDim.x + threadIdx.x];
		}

		// Wait for all threads to finish using current tiles before loading in new
		// ones
		__syncthreads();
	}

	// Write back results
	c[row * N + col] = tmp;
}



// Check result on the CPU
void verify_result(float *a, float *b, float *c) {
	// For every row...
	for (int i = 0; i < N; i++) {
		// For every column...
		for (int j = 0; j < N; j++) {
			// For every element in the row-column pair
			float tmp = 0;
			for (int k = 0; k < N; k++) {
				// Accumulate the partial results
				tmp += a[i * N + k] * b[k * N + j];
			}
			// Check against the CPU result
			assert(tmp == c[i * N + j]);
		}
	}
}


//Print matrix A(nr_rows_A, nr_cols_A) storage in column-major format
void print_matrix(const float *A, int N) {

	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			std::cout << A[j * N + i] << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}


//to determine number of thread for for - loop//
int dtn(int n, int min_n)
{
	int max_tn = n / min_n;
	const int g_ncore = omp_get_num_procs();
	int tn = max_tn > g_ncore ? g_ncore : max_tn;
	if (tn < 1)
	{
		tn = 1;
	}
	return tn;
}


//matrix multiplication by using openmp//
void omp_mm(float *a, int N, float *b, float *c)
{
	int i, j, k;
	int index;
	int border = N * N;
	i = 0;
	j = 0;

#pragma omp parallel for private(i,j,k) num_threads(dtn(border, 1))
	for (index = 0; index < border; index++)
	{
		i = index / N; j = index % N;
		int row_i = i * N;
		int row_c = i * N;
		c[row_c + j] = 0;
		for (k = 0; k < N; k++)
		{
			c[row_c + j] += a[row_i + k] * b[k*N + j];
		}
	}
}





int main() {

	//size of arrays in bytes
	size_t bytes = N * N * sizeof(float);

	// Allocate 5 arrays on CPU
	float *h_A = (float *)malloc(bytes);
	float *h_B = (float *)malloc(bytes);
	float *h_C = (float *)malloc(bytes);
	float *h_CC = (float *)malloc(bytes);
	float *h_CCC = (float *)malloc(bytes);

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	float gpuBLAS_elapsed_time_ms, cpu_elapsed_time_ms, gpu_elapsed_shared_time_ms;

	// Allocate 4 arrays on GPU
	float *d_A, *d_B, *d_C, *d_CC;
	cudaMalloc(&d_A, bytes);
	cudaMalloc(&d_B, bytes);
	cudaMalloc(&d_C, bytes);
	cudaMalloc(&d_CC, bytes);


	// Fill the arrays A and B on GPU with random numbers
	GPU_fill_rand(d_A, N);
	GPU_fill_rand(d_B, N);

	// Optionally we can copy the data back on CPU and print the arrays
	cudaMemcpy(h_A, d_A, bytes, cudaMemcpyDeviceToHost);
	cudaMemcpy(h_B, d_B, bytes, cudaMemcpyDeviceToHost);
	
	//Wyswietlanie macierzy A oraz B
	std::cout << "A =" << std::endl;
	//print_matrix(h_A, N);
	std::cout << "B =" << std::endl;
	//print_matrix(h_B, N);


	cudaEventRecord(start, 0);

	// Multiply A and B on GPU
	gpu_blas_mmul(d_A, d_B, d_C, N);
	
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&gpuBLAS_elapsed_time_ms, start, stop);

	// Copy (and print) the result on host memory
	cudaMemcpy(h_C, d_C, N * N * sizeof(float), cudaMemcpyDeviceToHost);

	printf("Time elapsed on matrix multiplication on GPU with CuBLAS: %f ms.\n", gpuBLAS_elapsed_time_ms);

	std::cout << "C =" << std::endl;
	//print_matrix(h_C, N);

	int THREADS = 32;

	int BLOCKS = N / THREADS;

	dim3 threads(THREADS, THREADS);
	dim3 blocks(BLOCKS, BLOCKS);

	cudaEventRecord(start, 0);
	matrixMul << < blocks, threads >> > (d_A, d_B, d_CC);
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&gpu_elapsed_shared_time_ms, start, stop);
	printf("Time elapsed on matrix multiplication on GPU with shared memory: %f ms.\n\n", gpu_elapsed_shared_time_ms);

	cudaMemcpy(h_CC, d_CC, bytes, cudaMemcpyDeviceToHost);

	cout << "Checking result..." << std::endl;
	verify_result(h_A, h_B, h_CC);
	cout << "done";
	std::cout << std::endl;

	clock_t pocz, kon;
	double czas;
	cudaEventRecord(start, 0);
	pocz = clock();
	omp_mm(h_A, N, h_B, h_CCC);
	kon = clock();
	czas = (kon - pocz) / CLOCKS_PER_SEC;
	cout << "zajelo " << czas;
	//cpu_matrix_mult(h_A, h_B, h_CCC, N);
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&cpu_elapsed_time_ms, start, stop);
	printf("Time elapsed on matrix multiplication of on CPU: %f ms.\n\n", cpu_elapsed_time_ms);

	//Free GPU memory
	cudaFree(d_A);
	cudaFree(d_B);
	cudaFree(d_C);

	// Free CPU memory
	free(h_A);
	free(h_B);
	free(h_C);
	free(h_CC);
	free(h_CCC);

	return 0;
}